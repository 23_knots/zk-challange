var assert = require('assert');
var meeting = require('../16/index.js');

describe('testy funkcji', function () {

    it('zwraca informacje o dostepnosci sali', function () {
        var result = meeting(['X', 'O', 'X']);

        assert.equal(result, 1, 'sprawdzono');
    });

    it('zwraca informacje o dostepnosci sali', function () {
        var result = meeting(['O','X','X','X','X']);

        assert.equal(result, 0, 'sprawdzono');
    });

    it('zwraca informacje o dostepnosci sali', function () {
        var result = meeting(['X','X','X','X','X']);

        assert.equal(result, 'Brak dostępnych sal!', 'sprawdzono');
    });

});