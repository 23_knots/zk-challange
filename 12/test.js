var assert = require('assert');
var rgbToHex = require('../12/index.js');

describe('testy funkcji', function () {

    it('zwraca wartosc szestnastkowa', function () {
        var result = rgbToHex(255, 0, 0);

        assert.equal(result, "#FF0000", 'przekonwertowano');
    });

    it('zwraca wartosc szestnastkowa', function () {
        var result = rgbToHex(0, 255, 0);

        assert.equal(result, "#00FF00", 'przekonwertowano');
    });

    it('zwraca wartosc szestnastkowa', function () {
        var result = rgbToHex(0, 0, 255);

        assert.equal(result, "#0000FF", 'przekonwertowano');
    });

    it('zwraca wartosc szestnastkowa', function () {
        var result = rgbToHex(0, 191, 255);

        assert.equal(result, "#00BFFF", 'przekonwertowano');
    });

    

});