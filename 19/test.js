var assert = require('assert');
var pyramid = require('../19/index.js');

describe('testy funkcji', function () {

    it('zwraca piramide', function () {
        var result = pyramid(1);

        assert.equal(result, "/\\\n", 'piramida');
    });

    it('zwraca piramide', function () {
        var result = pyramid(2);

        assert.equal(result, " /\\\n/__\\\n", 'piramida');
    });

    it('zwraca piramide', function () {
        var result = pyramid(4);

        assert.equal(result, "   /\\\n  /  \\\n /    \\\n/______\\\n", 'piramida');
    });

    it('zwraca piramide', function () {
        var result = pyramid(6);

        assert.equal(result, "     /\\\n    /  \\\n   /    \\\n  /      \\\n /        \\\n/__________\\\n", 'piramida');
    });

    it('zwraca piramide', function () {
        var result = pyramid(10);

        assert.equal(result, "         /\\\n        /  \\\n       /    \\\n      /      \\\n     /        \\\n    /          \\\n   /            \\\n  /              \\\n /                \\\n/__________________\\\n", 'piramida');
    });

});