var assert = require('assert');
var skiJump = require('../07/index.js');

describe('testy funkcji', function () {

    it('zwraca informacje nt. odleglosci skoku', function () {
        var result = skiJump(['*']);

        assert.equal(result, "1 metrów 35 centymetrów: To było słabe!", '1m 35cm');
    });

    it('zwraca informacje nt. odleglosci skoku', function () {
        var result = skiJump(['*', '**', '***']);

        assert.equal(result, "12 metrów 15 centymetrów: Było blisko dobrego skoku!", '12m 15cm');
    });

    it('zwraca informacje nt. odleglosci skoku', function () {
        var result = skiJump(['*', '**', '***', '****', '*****', '******']);

        assert.equal(result, "48 metrów 60 centymetrów: To było dobre!", '48m 60cm');
    });

    it('zwraca informacje nt. odleglosci skoku', function () {
        var result = skiJump(['*', '**', '***', '****', '*****', '******', '*******', '********']);

        assert.equal(result, "86 metrów 40 centymetrów: Mamy nowy rekord!", '86m 40cm');
    });

});