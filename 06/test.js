var assert = require('assert');
var printError = require('../06/index.js');

describe('testy funkcji', function () {

    it('zwraca informacje nt. ciagu kontrolnego', function () {
        var result = printError("aaabbbbhaijjjm");

        assert.equal(result, "0/14", '0/14');
    });

    it('zwraca informacje nt. ciagu kontrolnego', function () {
        var result = printError("aaaxbbbbyyhwawiwjjjwwm");

        assert.equal(result, "8/22", '8/22');
    });

    it('zwraca informacje nt. ciagu kontrolnego', function () {
        var result = printError("aaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz");

        assert.equal(result, "3/56", '3/56');
    });

});