var assert = require('assert');
var positiveSum = require('../03/index.js');

describe('testy funkcji', function () {

    it('zwraca odpowiednia nazwe pietra', function () {
        var result = switchLift({A:1, B:1, C:10}, 3);

        assert.equal(result, "A", 'Winda A');
    });

    it('zwraca odpowiednia nazwe pietra', function () {
        var result = switchLift({A:1, B:1, C:10}, 4);

        assert.equal(result, "B", 'Winda B');
    });

    it('zwraca odpowiednia nazwe pietra', function () {
        var result = switchLift({A:1, B:1, C:10}, 5);

        assert.equal(result, "A", 'Winda A');
    });

    it('zwraca odpowiednia nazwe pietra', function () {
        var result = switchLift({A:1, B:1, C:10}, 6);

        assert.equal(result, "C", 'Winda C');
    });

    it('zwraca odpowiednia nazwe pietra', function () {
        var result = switchLift({A:1, B:1, C:9}, 5);

        assert.equal(result, "AC", 'Winda A i C');
    });

    it('zwraca odpowiednia nazwe pietra', function () {
        var result = switchLift({A:1, B:1, C:10}, 6);

        assert.equal(result, "C", 'Winda C');
    });

    it('zwraca odpowiednia nazwe pietra', function () {
        var result = switchLift({A:5, B:4, C:6}, 1);

        assert.equal(result, "B", 'Winda B');
    });

    it('zwraca odpowiednia nazwe pietra', function () {
        var result = switchLift({A:1,B:1,C:10},1);

        assert.equal(result, "", 'Brak');
    });

});