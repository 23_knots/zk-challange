var assert = require('assert');
var getMiddle = require('../02/index.js');

describe('testy funkcji', function () {

    it('zwraca srodkowy znak / znaki z łancucha znaków "es"', function () {
        var result = getMiddle('test');

        assert.equal(result, 'es', 'getMiddle(\'test\') jest \'es\'');
    });

    it('zwraca srodkowy znak / znaki z łancucha znaków "ow"', function () {
        var result = getMiddle('testowanie');

        assert.equal(result, 'ow', 'getMiddle(\'testowanie\') jest \'ow\'');
    });

    it('zwraca srodkowy znak / znaki z łancucha znaków "nn"', function () {
        var result = getMiddle('anna');

        assert.equal(result, 'nn', 'getMiddle(\'anna\') jest \'nn\'');
    });

    it('zwraca srodkowy znak / znaki z łancucha znaków "z"', function () {
        var result = getMiddle('niedziela');

        assert.equal(result, 'z', 'getMiddle(\'niedziela\') jest \'z\'');
    });

    it('zwraca srodkowy znak / znaki z łancucha znaków "A"', function () {
        var result = getMiddle('A');

        assert.equal(result, 'A', 'getMiddle(\'A\') jest \'A\'');
    });

});