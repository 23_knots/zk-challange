var assert = require('assert');
var movie = require('../20/index.js');

describe('testy funkcji', function () {

    it('zwraca informacje o biletach', function () {
        var result = movie(500, 15, 0.9);

        assert.equal(result, 43, 'obliczono');
    });

    it('zwraca informacje o biletach', function () {
        var result = movie(100, 10, 0.95);

        assert.equal(result, 24, 'obliczono');
    });

});