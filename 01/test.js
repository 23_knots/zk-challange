var assert = require('assert');
var positiveSum = require('../01/index.js');

describe('testy funkcji', function () {

    it('zwraca sume dodatnich elementow z tablicy wynoszaca 15', function () {
        var result = positiveSum([1, 2, 3, 4, 5]);

        assert.equal(result, 15, 'positiveSum([1, 2, 3, 4, 5]) jest 15');
    });

    it('zwraca sume dodatnich elementow z tablicy wynoszaca 13', function () {
        var result = positiveSum([1, -2, 3, 4, 5]);

        assert.equal(result, 13, 'positiveSum([1,-2,3,4,5]) jest 13');
    });

    it('zwraca sume dodatnich elementow z tablicy wynoszaca 0', function () {
        var result = positiveSum([]);

        assert.equal(result, 0, 'positiveSum([]) jest 0');
    });

    it('zwraca sume dodatnich elementow z tablicy wynoszaca 0', function () {
        var result = positiveSum([-1, -2, -3, -4, -5]);

        assert.equal(result, 0, 'positiveSum([-1,-2,-3,-4,-5]) jest 0');
    });

    it('zwraca sume dodatnich elementow z tablicy wynoszaca 9', () => {
        let result = positiveSum([-1, 2, 3, 4, -5]);

        assert.equal(result, 9, 'positiveSum([-1,2,3,4,-5]) jest 9');
    });

});