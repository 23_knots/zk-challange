var assert = require('assert');
var encode = require('../18/index.js');

describe('testy funkcji', function () {

    it('zwraca informacje o wadze', function () {
        var result = encode("scout",1939);

        assert.equal(result, [20, 12, 18, 30, 21], 'zakodowano');
    });

    it('zwraca informacje o wadze', function () {
        var result = encode("masterpiece",1939);

        assert.equal(result, [14, 10, 22, 29, 6, 27, 19, 18, 6, 12, 8], 'zakodowano');
    });

});