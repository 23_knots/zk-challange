var assert = require('assert');
var generateSquare = require('../11/index.js');

describe('testy funkcji', function () {

    it('generuje kwadrat', function () {
        var result = generateSquare(8);

        assert.equal(result, "++++++++\n++++++++\n++++++++\n++++++++\n++++++++\n++++++++\n++++++++\n++++++++", 'wygenerowano');
    });

    it('generuje kwadrat', function () {
        var result = generateSquare(3);

        assert.equal(result, "+++\n+++\n+++", 'wygenerowano');
    });

    it('generuje kwadrat', function () {
        var result = generateSquare(0);

        assert.equal(result, "", 'wygenerowano');
    });

    it('generuje kwadrat', function () {
        var result = generateSquare(1);

        assert.equal(result, "+", 'wygenerowano');
    });

});