var assert = require('assert');
var makePassword = require('../05/index.js');

describe('testy funkcji', function () {

    it('zwraca prawidlowe haslo', function () {
        var result = makePassword("Idzie Grześ przez wieś");

        assert.equal(result, "1Gpw", 'Idzie Grześ przez wieś - 1Gpw');
    });

    it('zwraca prawidlowe haslo', function () {
        var result = makePassword("Wlazł kotek na płotek");

        assert.equal(result, "Wknp", 'Wlazł kotek na płotek - Wknp');
    });

    it('zwraca prawidlowe haslo', function () {
        var result = makePassword("Być albo nie być oto jest pytanie");

        assert.equal(result, "Banb0jp", 'Być albo nie być oto jest pytanie - Banb0jp');
    });

    it('zwraca prawidlowe haslo', function () {
        var result = makePassword("Litwo Ojczyzno Moja Ty...");

        assert.equal(result, "L0MT", 'Litwo Ojczyzno Moja Ty... - L0MT');
    });

    it('zwraca prawidlowe haslo', function () {
        var result = makePassword("Lorem Ipsum Dolor Sit Amet");

        assert.equal(result, "L1D5A", 'Lorem Ipsum Dolor Sit Amet - L1D5A');
    });

});