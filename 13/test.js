var assert = require('assert');
var population = require('../13/index.js');

describe('testy funkcji', function () {

    it('zwraca czas niezbedny do osiagniecia zamierzonej populacji', function () {
        var result = population(1500, 5, 100, 5000);

        assert.equal(result, 15, 'przeliczono');
    });

    it('zwraca czas niezbedny do osiagniecia zamierzonej populacji', function () {
        var result = population(1500000, 2.5, 10000, 2000000);

        assert.equal(result, 10, 'przeliczono');
    });

    it('zwraca czas niezbedny do osiagniecia zamierzonej populacji', function () {
        var result = population(1500000, 0.25, 1000, 2000000);

        assert.equal(result, 94, 'przeliczono');
    });

});