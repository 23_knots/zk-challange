var assert = require('assert');
var findShort = require('../09/index.js');

describe('testy funkcji', function () {

    it('zwraca dlugosc najkrotszego slowa', function () {
        var result = findShort("To że milczę nie znaczy że nie mam nic do powiedzenia");

        assert.equal(result, 2, '2 znaki');
    });

    it('zwraca dlugosc najkrotszego slowa', function () {
        var result = findShort("Lepiej późno niż później");

        assert.equal(result, 3, '3 znaki');
    });

    it('zwraca dlugosc najkrotszego slowa', function () {
        var result = findShort("Jeden dwa trzy cztery pięć sześć");

        assert.equal(result, 3, '3 znaki');
    });

});