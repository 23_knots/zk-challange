var assert = require('assert');
var bingo = require('../08/index.js');

describe('testy funkcji', function () {

    it('zwraca informacje nt. wygranej', function () {
        var result = bingo([21,13,2,7,5,14,7,15,9,10]);

        assert.equal(result, "Wygrana", 'BINGO');
    });

    it('zwraca informacje nt. wygranej', function () {
        var result = bingo([1,2,3,4,5,6,7,8,9,10]);

        assert.equal(result, "Przegrana", ':(');
    });

});