var assert = require('assert');
var targetDay = require('../04/index.js');

describe('testy funkcji', function () {

    it('zwraca oczekiwana date', function () {
        var result = targetDay(4281, 5087, 2);

        // assert.equal(result, "2027-07-05", '5 Lipca 2027');
        assert.equal(result, "2027-07-04", '4 Lipca 2027');
    });

    it('zwraca oczekiwana date', function () {
        var result = targetDay(4620, 5188, 2);

        // assert.equal(result, "2024-09-20", '20 Wrzesnia 2024');
        assert.equal(result, "2024-09-19", '19 Wrzesnia 2024');
    });

    it('zwraca oczekiwana date', function () {
        var result = targetDay(9999, 11427, 6);

        // assert.equal(result, "2021-03-14", '14 Marca 2021');
        assert.equal(result, "2021-03-13", '13 Marca 2021');
    });

    it('zwraca oczekiwana date', function () {
        var result = targetDay(3525, 4822, 3);

        // assert.equal(result, "2029-04-19", '19 Kwietnia 2029');
        assert.equal(result, "2029-04-18", '18 Kwietnia 2029');
    });

    it('zwraca oczekiwana date', function () {
        var result = targetDay(1244, 2566, 4);

        // assert.equal(result, "2036-11-05", '5 Listopada 2036');
        assert.equal(result, "2036-11-04", '4 Listopada 2036');
    });

});