var assert = require('assert');
var maskify = require('../10/index.js');

describe('testy funkcji', function () {

    it('maskuje ciag znakow', function () {
        var result = maskify("123456789");

        assert.equal(result, "#####6789", 'Zamaskowano');
    });

    it('maskuje ciag znakow', function () {
        var result = maskify("12345");

        assert.equal(result, "#2345", 'Zamaskowano');
    });

    it('maskuje ciag znakow', function () {
        var result = maskify("123");

        assert.equal(result, "123", 'Zamaskowano');
    });

    it('maskuje ciag znakow', function () {
        var result = maskify("");

        assert.equal(result, "", 'Zamaskowano');
    });

    it('maskuje ciag znakow', function () {
        var result = maskify("Coding Challenge");

        assert.equal(result, "############enge", 'Zamaskowano');
    });

});