var assert = require('assert');
var checkCoupon = require('../14/index.js');

describe('testy funkcji', function () {

    it('zwraca czy kupon jest wazny', function () {
        var result = checkCoupon('123','123','September 5, 2018','October 1, 2018');

        assert.equal(result, true, 'sprawdzono');
    });

    it('zwraca czy kupon jest wazny', function () {
        var result = checkCoupon('123a','123','September 5, 2018','October 1, 2018');

        assert.equal(result, false, 'sprawdzono');
    });

});